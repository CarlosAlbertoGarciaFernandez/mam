package Paquete;

import Paquete.Lineaorden;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-11-20T20:17:05")
@StaticMetamodel(Orden.class)
public class Orden_ { 

    public static volatile CollectionAttribute<Orden, Lineaorden> lineaordenCollection;
    public static volatile SingularAttribute<Orden, Date> fechaOrden;
    public static volatile SingularAttribute<Orden, Integer> idOrden;

}