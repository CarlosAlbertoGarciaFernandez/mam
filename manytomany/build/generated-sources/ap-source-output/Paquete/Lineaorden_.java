package Paquete;

import Paquete.LineaordenPK;
import Paquete.Orden;
import Paquete.Producto;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-11-20T20:17:05")
@StaticMetamodel(Lineaorden.class)
public class Lineaorden_ { 

    public static volatile SingularAttribute<Lineaorden, Orden> orden;
    public static volatile SingularAttribute<Lineaorden, Integer> cantidad;
    public static volatile SingularAttribute<Lineaorden, LineaordenPK> lineaordenPK;
    public static volatile SingularAttribute<Lineaorden, Producto> producto;

}