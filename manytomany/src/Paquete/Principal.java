/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Paquete;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author CarlosAlberto
 */
public class Principal {

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("manytomanyPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Lineaorden lo=new Lineaorden();
        LineaordenPK lopk=new LineaordenPK();
        
        lo.setCantidad(10);
        lopk.setIdOrden(3);
        lopk.setIdProducto(4);
        lo.setLineaordenPK(lopk);
        em.persist(lo);
        Collection<Producto> lista_producto;
        lista_producto = em.createNamedQuery("Producto.findAll").getResultList();
        System.out.println("Productos");
        for (Producto el : lista_producto) {
            System.out.println("-------------------------------------------------");
            System.out.println("Producto: " + el.getIdProducto() + " " + el.getDescripcion());
            System.out.println("Presio: $" + el.getPrecio());
        }
        System.out.println("-------------------------------------------------");
        Collection<Lineaorden> lista_lineaop;
        lista_lineaop = em.createNamedQuery("Lineaorden.findAll").getResultList();
        Orden o = em.find(Orden.class, 2);
        System.out.println("Ordenes");
        for (Lineaorden el : lista_lineaop) {
            if (!o.getIdOrden().equals(el.getOrden().getIdOrden())) {
                System.out.println("-------------------------------------------------");
                System.out.println("Orden: " + el.getOrden().getIdOrden());
                System.out.println("Fecha: " + el.getOrden().getFechaOrden());
            }
            System.out.println("Producto: " + el.getProducto().getIdProducto() + " " + el.getProducto().getDescripcion());
            System.out.println("Cantidad: " + el.getCantidad());
            System.out.println("Precio: $" + el.getProducto().getPrecio());
            o = el.getOrden();
        }
        System.out.println("-------------------------------------------------");
        try {
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        em.close();
        emf.close();
    }
}
